---
title: cross compile  
tags: [aarch]
---

sudo must be owned by uid 0 and have the setuid bit set
``` bash
chown root:root /usr/bin/sudo && chmod 4755 /usr/bin/sudo
```

sudo parted /dev/mmcblk1 resizepart 2 20000M
partprobe
sudo resize2fs /dev/mmcblk1p2 20000M

``` bash
sudo systemctrl enable ssh
sudo apt install netplan.io
```

dds

``` bash
sudo apt-get update
sudo apt install libasio-dev libtinyxml2-dev -y
sudo apt install libssl-dev
sudo apt install cmake -y
```

colcon for setup.zsh

moudles

```
sudo apt-get install libgtest-dev
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:~/Fast-DDS/install/lib
sudo apt-get install libncurses5-dev
```

``` bash
source ~/core-sdk/environment-setup-aarch64-poky-linux
```

sdk
```
sudo apt update
sudo apt install python-is-python2
```

``` dockerfile
FROM ubuntu:19.04

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y && \
apt-get install --no-install-recommends -y vim gawk wget git-core diffstat unzip texinfo gcc \
         build-essential chrpath socat libsdl1.2-dev xterm
RUN apt-get install -y  cpio file 
RUN apt update && apt install -y zstd liblz4-tool
RUN apt-get clean && apt-get update && apt-get install -y locales
RUN locale-gen en_US.UTF-8
RUN apt-get install --reinstall ca-certificates
```
yum install lz4 zstd
docker run -u 0 -it vas bash
set BB_NO_NETWORK=1

```
apt-get install --reinstall ca-certificates
mkdir /usr/local/share/ca-certificates/cacert.org
wget -P /usr/local/share/ca-certificates/cacert.org http://www.cacert.org/certs/root.crt http://www.cacert.org/certs/class3.crt
update-ca-certificates
git config --global http.sslCAinfo /etc/ssl/certs/ca-certificates.crt
```

```
sudo apt install -y liblz4-dev
git clone http://git.yoctoproject.org/git/poky
```
source ./oe

core-image-rt was skipped: Set PREFERRED_PROVIDER_virtual/kernel to linux-yocto-rt to enable it
build/conf/local.conf
	PREFERRED_PROVIDER_virtual/kernel= "linux-yocto-rt"
	MACHINE??="qemuarm64"

bitbake -k core-image-rt-sdk
bitbake -c populate_sdk core-image-rt-sdk

docker pull

docker run -it -v yoctovolume:/workdir gmacario/build-yocto
docker run -it --rm -v yoctovolume:/workdir gmacario/build-yocto sudo chown -R build:build /workdir

docker images -a
docker rmi c2d70a12ac3f -f
docker cp foo.txt container_id:/foo.txt
docker run -u 0 -it mycontainer bash ## login as root

docker volume ls

sudo vi /media/<username>/bootfs/mmc0_extlinux/extlinux.conf

cmake -DDCMAKE_TOOLCHAIN_FILE=./setting.cmake ..

setting.cmake

``` cmake
et(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)

set(CMAKE_C_COMPILER $CC)
set(CMAKE_CXX_COMPILER $CXX)
```

dds

``` bash
git clone https://github.com/OpenEtherCATsociety/SOEM.git
cd Fast-DDS
git clone 
```
