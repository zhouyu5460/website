---
title: EtherCat  
tags: [EtherCat, Colpley, SOEM]
---
## Note
this makes keybord input not work
``` bash
echo "password" | sudo -S simple_test en7
```
## FFMU SM

##
Difference Between Trapezoidal/Trajectory and S-Curve Profiles

|     | Trajectory | S-Curve | 
| :-- | ---------- | ------- | 
|     |            | Smooth  | 


## Colpley PDO Mapping using SOEM


``` c
int copleySetup(uint16 slave)
{
    int retval;
    uint16 u16val =0;
    uint8 u8val=0;
    retval = 0;

    // Rx PDO
    retval = ec_SDOwrite(slave,0x1C12,0x00,FALSE,sizeof(u8val),&u8val,EC_TIMEOUTRXM);
    printf("ret1: %d\n", retval);

    u16val = 0x1700;
    retval = ec_SDOwrite(slave,0x1C12,0x01,FALSE,sizeof(u16val),&u16val,EC_TIMEOUTRXM);
    printf("ret5: %d\n", retval);
    
    u8val = 1;
    retval = ec_SDOwrite(slave,0x1C12,0x00,FALSE,sizeof(u8val),&u8val,EC_TIMEOUTRXM);
    printf("ret6: %d\n", retval);

    return 1;
}
```

```c
void main()
{
    ...
    
    ec_config_init(FALSE);

    ec_slave[1].PO2SOconfig = copleySetup;

    ec_config_map(&IOmap);
    
    ...
} 

```