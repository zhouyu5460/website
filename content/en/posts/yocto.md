---
title: yocto
tags: [bitbake, docker]
---

## docker

run
``` bah
docker images 
docker run -it vas
docker run -u 0 -it mycontainer bash ## login as root
```

build
``` bash
docker build -t vas .
docker pull
docker rmi c2d70a12ac3f -f
```

copy
``` bash
docker container ls
docker cp foo.txt container_id:/foo.txt
```

volume
``` bash
docker volume ls
docker run -it -v yoctovolume:/workdir gmacario/build-yocto
docker run -it --rm -v yoctovolume:/workdir gmacario/build-yocto sudo chown -R build:build /workdir
```

``` dockerfile
FROM arm64v8/ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive
ENV TERM xterm

RUN apt-get update -y && \
apt-get install --no-install-recommends -y vim gawk wget git-core diffstat unzip texinfo gcc \
         build-essential chrpath socat libsdl1.2-dev xterm
RUN apt-get install -y  cpio file
RUN apt install -y zstd liblz4-tool
RUN apt-get install -y locales
RUN locale-gen en_US.UTF-8
RUN apt-get install -y ca-certificates
RUN mkdir /usr/local/share/ca-certificates/cacert.org
RUN wget -P /usr/local/share/ca-certificates/cacert.org http://www.cacert.org/certs/root.crt http://www.cacert.org/certs/class3.crt
RUN update-ca-certificates
RUN git config --global http.sslCAinfo /etc/ssl/certs/ca-certificates.crt

RUN useradd --create-home --shell /bin/bash vas
USER vas
WORKDIR /home/vas
```

## yocto

``` bash
git clone git://git.yoctoproject.org/poky
bitbake-layers add-layer ../meta-openembedded/meta-oe/
```

## extend sd card
``` bash
sudo parted /dev/mmcblk1 resizepart 2 20000M
partprobe
sudo resize2fs /dev/mmcblk1p2 20000M
```
